//Include our classes
#include "Paddle.h"

Paddle::Paddle(){
	mRect.x = 0;
	mRect.y = 0;
	mRect.w = 0;
	mRect.h = 0;
}

Paddle::~Paddle(){

}

void Paddle::init(int x, int y, int w, int h) {
	mRect.x = x;
	mRect.y = y;
	mRect.w = w;
	mRect.h = h;
}

void Paddle::update(){
	mRect.y = mRect.y + mSpeedY*(int)global_delta_time/1000;
	if (mRect.y < 0) { mRect.y = 0; }

	if (mRect.y + mRect.h >= SCREEN_HEIGHT) {
		mRect.y = SCREEN_HEIGHT - mRect.h;
	}


}
void Paddle::render(){
	ofSetColor(255, 255, 255);
	ofDrawRectangle(mRect.x, mRect.y, mRect.w, mRect.h);
}

void Paddle::setSpeedY(int spd) {
	mSpeedY = spd;
	return;
}

int Paddle::getSpeedY() {
	 return mSpeedY;
}

C_Rectangle Paddle::getRect() {
	return mRect;
}