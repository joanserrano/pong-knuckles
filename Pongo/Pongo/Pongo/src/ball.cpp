//Include our classes
#include "ball.h"

Ball::Ball(){
	mRect.x = 0;
	mRect.y = 0;
	mRect.w = 0;
	mRect.h = 0;
	mSpeedY = 0;
	mSpeedX = 0;
}

Ball::~Ball(){

}

void Ball::init(int x, int y, int w, int h) {
	mRect.x = x;
	mRect.y = y;
	mRect.w = w;
	mRect.h = h;
}

void Ball::update(){
	mRect.y = mRect.y + mSpeedY*(int)global_delta_time / 1000;

	if (mRect.y < 0 || mRect.y + mRect.h >= SCREEN_HEIGHT) {
		mSpeedY = -mSpeedY;
	}

	if (mRect.y < 0) { mRect.y = 0; }

	if (mRect.y + mRect.h >= SCREEN_HEIGHT) {
		mRect.y = SCREEN_HEIGHT - mRect.h;
	}

	mRect.x = mRect.x + mSpeedX*(int)global_delta_time / 1000;
	if (mRect.x < 0) { mRect.x = 0; }

	if (mRect.x + mRect.w >= SCREEN_WIDTH) {
		mRect.x = SCREEN_WIDTH - mRect.w;
	}
}

void Ball::render(){
	ofSetColor(255, 255, 255);
	ofDrawRectangle(mRect.x, mRect.y, mRect.w, mRect.h);
}

void Ball::setSpeedY(int spd) {
	mSpeedY = spd;
	return;
}

int Ball::getSpeedY() {
	 return mSpeedY;
}

void Ball::setSpeedX(int spd) {
	mSpeedX = spd;
	return;
}

int Ball::getSpeedX() {
	return mSpeedX;
}

C_Rectangle Ball::getRect() {
	return mRect;
}