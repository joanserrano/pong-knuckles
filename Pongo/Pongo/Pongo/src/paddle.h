#ifndef PADEL_H
#define PADEL_H

#include "includes.h"
#include "ofMain.h"

class Paddle
{
	public:
		Paddle();
		~Paddle();

		void init(int x, int y, int w, int h);

		void update();
		void render();

		void setSpeedY(int spd);
		int getSpeedY();

		C_Rectangle getRect();

	protected:
	
	private:
		C_Rectangle mRect;
		int mSpeedY;
	
};

#endif
