#ifndef BALL_H
#define BALL_H

#include "includes.h"
#include "ofMain.h"

class Ball
{
	public:
		Ball();
		~Ball();

		void init(int x, int y, int w, int h);

		void update();
		void render();

		void setSpeedY(int spd);
		int getSpeedY();

		void setSpeedX(int spd);
		int getSpeedX();

		C_Rectangle getRect();

	protected:
	
	private:
		C_Rectangle mRect;
		int mSpeedY;
		int mSpeedX;
	
};

#endif
