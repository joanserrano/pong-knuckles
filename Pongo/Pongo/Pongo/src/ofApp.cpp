#include "ofApp.h"
#include "includes.h"
#include "Utils.h"

//Reloj
const clock_t begin_time = clock();
clock_t new_time;
clock_t old_time;

//Variables globales
unsigned int global_delta_time = 0;
int timeCollision = 0;

int score1 = 0;
int score2 = 0;
int scoreV = 5;

int highscore;

std::fstream file;
std::fstream file2;

bool key_pressed[255];
bool key_down[255];
bool key_released[255];

//--------------------------------------------------------------
void ofApp::setup(){
	//Time
	ofSetFrameRate(60);
	old_time = begin_time;
	new_time = begin_time;

	//Control
	for (int i = 0; i < 255; i++) {
		key_down[i] = false;
		key_pressed[i] = false;
		key_released[i] = false;
	}
	//font
	score.load("Orbitron-Black.ttf", 36);

	mPlayer1 = new Paddle();
	mPlayer2 = new Paddle();
	mBall = new Ball();

	mPlayer1->init(20, SCREEN_HEIGHT / 2 - 50, 20, 100);
	mPlayer2->init(SCREEN_WIDTH - 40, SCREEN_HEIGHT/2-50, 20, 100);
	mBall->init(SCREEN_WIDTH / 2 - 10, SCREEN_HEIGHT / 2, 20, 20);

	mBall->setSpeedX(200);
	mBall->setSpeedY(0);

	//Highscore

	file.open("hscore.txt", std::ios::in);
	if (file.is_open()) {
		file.read((char*)&highscore, sizeof(int));
	}
	else {
		highscore = 0;
	}
	file.close();
}

//--------------------------------------------------------------
void ofApp::update(){
	//Begin update
	//Delta time update (has to be done always, errors could happen otherwise)
	old_time = new_time;
	new_time = clock() - begin_time;
	global_delta_time = int(new_time - old_time);
	//Update
	if (key_down['s'] || key_down['S']) {
		mPlayer1->setSpeedY(500);
	}
	else if (key_down['w'] || key_down['W']) {
		mPlayer1->setSpeedY(-500);
	}
	else {
		mPlayer1->setSpeedY(0);
	}

	if (key_down['l'] || key_down['L']) {
		mPlayer2->setSpeedY(500);
	}
	else if (key_down['o'] || key_down['O']) {
		mPlayer2->setSpeedY(-500);
	}
	else {
		mPlayer2->setSpeedY(0);
	}

	C_Rectangle rect1 = mPlayer1->getRect();
	C_Rectangle rect2 = mPlayer2->getRect();
	C_Rectangle rectb = mBall->getRect();
	bool col1 = C_RectangleCollision(rect1, rectb);
	bool col2 = C_RectangleCollision(rect2, rectb);

	if (timeCollision > 0) {
		timeCollision -= global_delta_time;
		if (timeCollision < 0) { timeCollision = 0; }
	}

	if ((col1 || col2) == true && timeCollision == 0) {
		
		if (col1 == true) {
			if (rectb.y + (rectb.h / 2) <= rect1.y + 40) {
				mBall->setSpeedY(-200);
			}
			else if (rectb.y + (rectb.h / 2) > rect1.y + 40 && rectb.y + (rectb.h / 2) < rect1.y + 60) {
				mBall->setSpeedY(0);
			}
			else if (rectb.y + (rectb.h / 2) >= rect1.y + 60) {
				mBall->setSpeedY(200);
			}
		}

		if (col2 == true) {
			if (rectb.y + (rectb.h / 2) <= rect1.y + 40) {
				mBall->setSpeedY(-200);
			}
			else if (rectb.y + (rectb.h / 2) > rect1.y + 40 && rectb.y + (rectb.h / 2) < rect1.y + 60) {
				mBall->setSpeedY(0);
			}
			else if (rectb.y + (rectb.h / 2) >= rect1.y + 60) {
				mBall->setSpeedY(200);
			}
		}

		mBall->setSpeedX(mBall->getSpeedX() * 1.1);
		mBall->setSpeedX(mBall->getSpeedX() * -1);
		timeCollision = 200;
	}

	if ((mBall->getSpeedX()) >= 1000) {
		mBall->setSpeedX(1000);
	}

	if (rectb.x <= 0) {
		score2++;
		scoreV++;
		mBall->init(SCREEN_WIDTH / 2 - 10, SCREEN_HEIGHT / 2, 20, 20);

		mBall->setSpeedX(-200);
		mBall->setSpeedY(0);
	}

	if (rectb.x + rectb.w >= SCREEN_WIDTH) {
		score1++;
		scoreV--;
		mBall->init(SCREEN_WIDTH / 2 - 10, SCREEN_HEIGHT / 2, 20, 20);

		mBall->setSpeedX(200);
		mBall->setSpeedY(0);
	}

	mPlayer1->update();

	mPlayer2->update();

	mBall->update();

	//End Update
	for (int i = 0; i < 255; i++) {
		key_pressed[i] = false;
		key_released[i] = false;
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofClear(0, 0, 0); //Limpiar pantalla

	//------Draw Game-------
	switch (scoreV) {
	case 0:
		ofSetColor(0, 0, 255, 100);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 4, 0, 85, SCREEN_HEIGHT);
		ofSetColor(0, 0, 255, 90);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 3, 0, 85, SCREEN_HEIGHT);
		ofSetColor(0, 0, 255, 80);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 2, 0, 85, SCREEN_HEIGHT);
		ofSetColor(0, 0, 255, 70);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 1, 0, 85, SCREEN_HEIGHT);
		ofSetColor(0, 0, 255, 60);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 0, 0, 85, SCREEN_HEIGHT);
		ofSetColor(255, 255, 255);
		score.drawString("WIN", 125, SCREEN_HEIGHT / 2);
		score.drawString("LOSE", SCREEN_WIDTH / 2 + 125, SCREEN_HEIGHT / 2);
		mBall->setSpeedX(0);
		mBall->setSpeedY(0);

		if (score1 > highscore) {
			ofSetColor(255, 255, 255);
			score.drawString("New Highscore!", SCREEN_WIDTH / 2 - 200, SCREEN_HEIGHT / 2 + 200);
			file.open("hscore.txt", std::ios::out | std::ios::trunc);
			file.write((char*)&score1, sizeof(int));
			file.close();
		}

		break;
	case 1:
		ofSetColor(0, 0, 255, 100);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 4, 0, 85, SCREEN_HEIGHT);
		ofSetColor(0, 0, 255, 90);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 3, 0, 85, SCREEN_HEIGHT);
		ofSetColor(0, 0, 255, 80);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 2, 0, 85, SCREEN_HEIGHT);
		ofSetColor(0, 0, 255, 70);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 1, 0, 85, SCREEN_HEIGHT);
		break;
	case 2:
		ofSetColor(0, 0, 255, 100);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 4, 0, 85, SCREEN_HEIGHT);
		ofSetColor(0, 0, 255, 90);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 3, 0, 85, SCREEN_HEIGHT);
		ofSetColor(0, 0, 255, 80);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 2, 0, 85, SCREEN_HEIGHT);
		break;
	case 3:
		ofSetColor(0, 0, 255, 100);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 4, 0, 85, SCREEN_HEIGHT);
		ofSetColor(0, 0, 255, 90);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 3, 0, 85, SCREEN_HEIGHT);
		break;
	case 4:
		ofSetColor(0, 0, 255, 100);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 4, 0, 85, SCREEN_HEIGHT);
		break;
	case 5:
		
		break;
	case 6:
		ofSetColor(255, 0, 0, 100);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 5, 0, 85, SCREEN_HEIGHT);
		break;
	case 7:
		ofSetColor(255, 0, 0, 100);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 5, 0, 85, SCREEN_HEIGHT);
		ofSetColor(255, 0, 0, 90);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 6, 0, 85, SCREEN_HEIGHT);
		break;
	case 8:
		ofSetColor(255, 0, 0, 100);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 5, 0, 85, SCREEN_HEIGHT);
		ofSetColor(255, 0, 0, 90);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 6, 0, 85, SCREEN_HEIGHT);
		ofSetColor(255, 0, 0, 80);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 7, 0, 85, SCREEN_HEIGHT);
		break;
	case 9:
		ofSetColor(255, 0, 0, 100);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 5, 0, 85, SCREEN_HEIGHT);
		ofSetColor(255, 0, 0, 90);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 6, 0, 85, SCREEN_HEIGHT);
		ofSetColor(255, 0, 0, 80);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 7, 0, 85, SCREEN_HEIGHT);
		ofSetColor(255, 0, 0, 70);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 8, 0, 85, SCREEN_HEIGHT);
		break;
	case 10:
		ofSetColor(255, 0, 0, 100);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 5, 0, 85, SCREEN_HEIGHT);
		ofSetColor(255, 0, 0, 90);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 6, 0, 85, SCREEN_HEIGHT);
		ofSetColor(255, 0, 0, 80);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 7, 0, 85, SCREEN_HEIGHT);
		ofSetColor(255, 0, 0, 70);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 8, 0, 85, SCREEN_HEIGHT);
		ofSetColor(255, 0, 0, 60);
		ofDrawRectangle(SCREEN_WIDTH / 10 * 9, 0, 85, SCREEN_HEIGHT);
		ofSetColor(255, 255, 255);
		score.drawString("LOSE", 125, SCREEN_HEIGHT / 2);
		score.drawString("WIN", SCREEN_WIDTH / 2 + 125, SCREEN_HEIGHT / 2);
		mBall->setSpeedX(0);
		mBall->setSpeedY(0);

		if (score2 > highscore) {
			ofSetColor(255, 255, 255);
			score.drawString("New Highscore!", SCREEN_WIDTH / 2 - 200, SCREEN_HEIGHT / 2 + 200);
			file.open("hscore.txt", std::ios::out | std::ios::trunc);
			file.write((char*)&score2, sizeof(int));
			file.close();
		}
		break;
	default:
		break;
	}

	ofSetColor(125, 0, 125, 100);
	ofDrawRectangle(SCREEN_WIDTH / 2 - 10, 0, 20, SCREEN_HEIGHT);
	ofSetColor(255, 255, 255);

	score.drawString(ofToString(score1), SCREEN_WIDTH/2 - 150, 40);
	score.drawString(ofToString(score2), SCREEN_WIDTH/2 + 100, 40);

	mPlayer1->render();
	mPlayer2->render();
	mBall->render();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key >= 255 || key < 0) { return; }
	if (!key_down[key]) {
		key_pressed[key] = true;
	}
	key_down[key] = true;
	key_released[key] = false;
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	if (key >= 255 || key < 0) { return; }
	key_pressed[key] = false;
	key_down[key] = false;
	key_released[key] = true;
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
	ofSetWindowShape(SCREEN_WIDTH, SCREEN_HEIGHT);
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
